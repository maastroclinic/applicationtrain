FROM rocker/r-base

RUN apt-get update
RUN apt-get install -t unstable -y libcurl4-openssl-dev
RUN apt-get install -t unstable -y libssl-dev

#run installation of needed R packages
COPY setup.r /setup.r
RUN Rscript setup.r

#Add analysis files
COPY query.sparql query.sparql
COPY analysis.r analysis.r
COPY run.sh run.sh

RUN touch /input.txt
RUN touch /output.txt

CMD ["sh", "run.sh"]
