#set generic properties
endpointUrl <- "http://localhost:8080/blazegraph/sparql"

#read sparql endpoint URL form environment variable
if(Sys.getenv("SPARQL_ENDPOINT")!="") {
  endpointUrl <- Sys.getenv("SPARQL_ENDPOINT")
}

#load libraries
library(sparqlr)

#######################################
# Read SPARQL endpoint information
#######################################

query <- paste(readLines("query.sparql"), collapse = " ")
result <- performSparqlQuery(endpointUrl, query)

#convert dateTime to date
result$date <- as.Date(result$date)

#filter on dates before 2018-01-01 and after/equal 2017-01-01
result <- result[result$date<as.Date("2018-01-01"),]
result <- result[result$date>=as.Date("2017-01-01"),]

patients <- unique(result$bsn)
patientActivities <- sapply(patients, function(x) { nrow(result[result$bsn==x,]) })

resultStrings <- c(paste0("Patients: ", length(patients)),
                   paste0("Mean activities: ", mean(patientActivities)),
                   paste0("Std. dev. activities: ", sd(patientActivities)),
                   paste0("total activities: ", sum(patientActivities)) )

fileConn<-file("output.txt")
writeLines(resultStrings, fileConn)
close(fileConn)